#!/usr/bin/python
import datetime
import requests

from time import time

from conf import device, config
from helpers import googleplay_pb2, login_helper


class LoginError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


def login(email, password):
    if email is not None and password is not None:
        print "%s - Trying to log in with username %s" % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), email)
        encrypted_password = login_helper.encrypt_password(email, password).decode('utf-8')
        params = get_auth_params(email, encrypted_password)

        if "auth" in params:
            google_service_framework_id = checkin(email, params["auth"])
            print "%s - Received Google Services Framework Identifier: %s" % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), str(google_service_framework_id))

            token = getAuthSubToken(email, encrypted_password)
            print "%s - Received auth token: %s" % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), token)

            uploadDeviceConfig(google_service_framework_id, token)
            return {
                'gsfid': google_service_framework_id,
                'token': token
            }
        elif "error" in params:
            if "NeedsBrowser" in params["error"]:
                print "%s - [ERROR] Security check is needed! " \
                      "Try to visit https://accounts.google.com/b/0/DisplayUnlockCaptcha to unlock." % \
                    datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                raise LoginError("Security check is needed, try to visit "
                                 "https://accounts.google.com/b/0/DisplayUnlockCaptcha "
                                 "to unlock, or setup an app-specific password")
            print "%s - [ERROR] Something went wrong!" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            raise LoginError("server says: " + params["error"])
        else:
            print "%s - [ERROR] Auth token not received!" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            raise LoginError("ERROR - Auth token not received.")
    else:
        print "%s - [ERROR] Email and password must be filled" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')


def get_auth_params(email, encrypted_password):
    params = getLoginParams(email, encrypted_password)
    response = requests.post(config.AUTH_URL, data=params, verify=True)
    data = response.text.split()
    params = {}
    for d in data:
        if "=" not in d:
            continue
        k, v = d.split("=", 1)
        params[k.strip().lower()] = v.strip()
    return params


def checkin(email, ac2dmToken):
    headers = getLoginHeaders()
    headers["Content-Type"] = "application/x-protobuffer"

    request = getAndroidCheckinRequest()

    stringRequest = request.SerializeToString()
    res = requests.post(config.CHECK_IN_URL, data=stringRequest, headers=headers, verify=True)
    response = googleplay_pb2.AndroidCheckinResponse()
    response.ParseFromString(res.content)

    request2 = googleplay_pb2.AndroidCheckinRequest()
    request2.CopyFrom(request)
    request2.id = response.androidId
    request2.securityToken = response.securityToken
    request2.accountCookie.append("[" + email + "]")
    request2.accountCookie.append(ac2dmToken)
    stringRequest = request2.SerializeToString()

    requests.post(config.CHECK_IN_URL, data=stringRequest, headers=headers, verify=True)
    return response.androidId


def getAuthSubToken(email, passwd):
    requestParams = getAuthParams(email, passwd)
    response = requests.post(config.AUTH_URL, data=requestParams, verify=True)
    data = response.text.split()
    params = {}
    for d in data:
        if "=" not in d:
            continue
        k, v = d.split("=", 1)
        params[k.strip().lower()] = v.strip()
    if "token" in params:
        firstToken = params["token"]
        secondToken = getSecondRoundToken(requestParams, firstToken)
        return secondToken
    elif "error" in params:
        print "%s - [ERROR] something went wrong!" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        raise LoginError("server says: " + params["error"])
    else:
        print "%s - [ERROR] no auth token received" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        raise LoginError("Auth token not found.")


def getSecondRoundToken(previousParams, firstToken):
    previousParams['Token'] = firstToken
    previousParams['service'] = 'androidmarket'
    previousParams['check_email'] = '1'
    previousParams['token_request_options'] = 'CAA4AQ=='
    previousParams['system_partition'] = '1'
    previousParams['_opt_is_called_from_account_manager'] = '1'
    previousParams['google_play_services_version'] = '11518448'
    previousParams.pop('Email')
    previousParams.pop('EncryptedPasswd')
    response = requests.post(config.AUTH_URL, data=previousParams, verify=True)
    data = response.text.split()
    params = {}
    for d in data:
        if "=" not in d:
            continue
        k, v = d.split("=", 1)
        params[k.strip().lower()] = v.strip()
    if "auth" in params:
        return params["auth"]
    elif "error" in params:
        print "%s - [ERROR] something went wrong!" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        raise LoginError("server says: " + params["error"])
    else:
        print "%s - [ERROR] no auth token received" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        raise LoginError("Auth token not found.")


def uploadDeviceConfig(gsfId, authSubToken):
    upload = googleplay_pb2.UploadDeviceConfigRequest()
    upload.deviceConfiguration.CopyFrom(getDeviceConfig())
    headers = getLoginHeaders()
    headers["X-DFE-Device-Id"] = "{0:x}".format(gsfId)
    headers["Authorization"] = "GoogleLogin auth=%s" % authSubToken

    headers["X-DFE-Enabled-Experiments"] = "cl:billing.select_add_instrument_by_default"
    headers["X-DFE-Unsupported-Experiments"] = "nocache:billing.use_charging_poller,market_emails,buyer_currency,prod_baseline,checkin.set_asset_paid_app_field,shekel_test,content_ratings,buyer_currency_in_app,nocache:encrypted_apk,recent_changes"
    headers["X-DFE-Client-Id"] = "am-android-google"
    headers["X-DFE-SmallestScreenWidthDp"] = "320"
    headers["X-DFE-Filter-Level"] = "3"
    stringRequest = upload.SerializeToString()
    res = requests.post(config.DEVICE_CONF_UPLOAD_URL, data=stringRequest, headers=headers, verify=True)
    googleplay_pb2.ResponseWrapper.FromString(res.content)


def getLoginParams(email, encryptedPass):
    return {"Email": email,
            "EncryptedPasswd": encryptedPass,
            "accountType": "HOSTED_OR_GOOGLE",
            "has_permission": "1",
            "source": "android",
            "device_country": "en",
            "lang": "en_US",
            "sdk_version": device.Build_VERSION_SDK_INT,
            "service": "ac2dm",
            "app": "com.google.android.gsf",
            "add_account": "1",
            }


def getAuthParams(email, passwd):
    return {"Email": email,
            "EncryptedPasswd": passwd,
            "accountType": "HOSTED_OR_GOOGLE",
            "has_permission": "1",
            "source": "android",
            "device_country": "en",
            "lang": "en_US",
            "sdk_version": device.Build_VERSION_SDK_INT,
            "service": "androidmarket",
            "app": "com.android.vending",
            }


def getUserAgent():
    return ("Android-Finsky/8.1.72.S-all [6] [PR] 165478484 ("
            "api=3"
            ",versionCode={versionCode}"
            ",sdk={sdk}"
            ",device={device}"
            ",hardware={hardware}"
            ",product={product}"
            "").format(versionCode=device.Vending_version,
                       sdk=device.Build_VERSION_SDK_INT,
                       device=device.Build_DEVICE,
                       hardware=device.Build_HARDWARE,
                       product=device.Build_PRODUCT)


def getLoginHeaders():
    return {"Accept-Language": "en-US",
            "X-DFE-Encoded-Targets": config.DFE_TARGETS,
            "User-Agent": getUserAgent()}


def getAndroidCheckinRequest():
    request = googleplay_pb2.AndroidCheckinRequest()
    request.id = 0
    request.checkin.CopyFrom(getAndroidCheckin())
    request.locale = "en_US"
    request.timeZone = "Europe/Berlin"
    request.version = 3
    request.deviceConfiguration.CopyFrom(getDeviceConfig())
    request.fragment = 0
    return request


def getAndroidCheckin():
    androidCheckin = googleplay_pb2.AndroidCheckinProto()
    androidCheckin.build.CopyFrom(getAndroidBuild())
    androidCheckin.lastCheckinMsec = 0
    androidCheckin.cellOperator = device.CellOperator
    androidCheckin.simOperator = device.SimOperator
    androidCheckin.roaming = device.Roaming
    androidCheckin.userNumber = 0
    return androidCheckin


def getAndroidBuild():
    androidBuild = googleplay_pb2.AndroidBuildProto()
    androidBuild.id = device.Build_FINGERPRINT
    androidBuild.product = device.Build_HARDWARE
    androidBuild.carrier = device.Build_BRAND
    androidBuild.radio = device.Build_RADIO
    androidBuild.bootloader = device.Build_BOOTLOADER
    androidBuild.device = device.Build_DEVICE
    androidBuild.sdkVersion = int(device.Build_VERSION_SDK_INT)
    androidBuild.model = device.Build_MODEL
    androidBuild.manufacturer = device.Build_MANUFACTURER
    androidBuild.buildProduct = device.Build_PRODUCT
    androidBuild.client = device.Client
    androidBuild.otaInstalled = False
    androidBuild.timestamp = int(time()/1000)
    androidBuild.googleServices = int(device.GSF_version)
    return androidBuild


def getDeviceConfig():
    libList = device.SharedLibraries.split(",")
    featureList = device.Features.split(",")
    localeList = device.Locales.split(",")
    glList = device.GL_Extensions.split(",")
    platforms = device.Platforms.split(",")

    hasFiveWayNavigation = (device.HasFiveWayNavigation == 'true')
    hasHardKeyboard = (device.HasHardKeyboard == 'true')
    deviceConfig = googleplay_pb2.DeviceConfigurationProto()
    deviceConfig.touchScreen = int(device.TouchScreen)
    deviceConfig.keyboard = int(device.Keyboard)
    deviceConfig.navigation = int(device.Navigation)
    deviceConfig.screenLayout = int(device.ScreenLayout)
    deviceConfig.hasHardKeyboard = hasHardKeyboard
    deviceConfig.hasFiveWayNavigation = hasFiveWayNavigation
    deviceConfig.screenDensity = int(device.Screen_Density)
    deviceConfig.screenWidth = int(device.Screen_Width)
    deviceConfig.screenHeight = int(device.Screen_Height)
    deviceConfig.glEsVersion = int(device.GL_Version)
    for x in platforms:
        deviceConfig.nativePlatform.append(x)
    for x in libList:
        deviceConfig.systemSharedLibrary.append(x)
    for x in featureList:
        deviceConfig.systemAvailableFeature.append(x)
    for x in localeList:
        deviceConfig.systemSupportedLocale.append(x)
    for x in glList:
        deviceConfig.glExtension.append(x)
    return deviceConfig