#!/usr/bin/python
import datetime
import requests

from conf import config
from helpers import request_helper, mapping_helper


def details(packageName, gsfId, authSubToken):
    if not valid_params(packageName, gsfId, authSubToken):
        return

    headers = request_helper.getDefaultHeaders(gsfId, authSubToken)
    url = config.DETAILS_URL + "?doc=%s" % requests.utils.quote(packageName)
    data = request_helper.executeRequestApi2(url, headers)

    return mapping_helper.document_to_dictionary(data.payload.detailsResponse.docV2)


def get_version_number(packageName, gsfId, authSubToken):
    return details(packageName, gsfId, authSubToken)['versionCode']


def valid_params(packageName, gsfId, authSubToken):
    print "%s - Validating details API params" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    are_params_valid = True
    if packageName is None:
        are_params_valid = False
        print "%s - Package name has to be defined to request details" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    if gsfId is None:
        are_params_valid = False
        print "%s - Google Services Framework Identifier (gsfId) must be defined to request details" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    if authSubToken is None:
        are_params_valid = False
        print "%s - Auth token must be defined to request details" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    return are_params_valid