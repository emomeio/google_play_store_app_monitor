#!/usr/bin/python
import datetime
import requests

from conf import config
from details import get_version_number
from helpers import googleplay_pb2
from helpers import request_helper


def download(packageName, gsfId, authSubToken, versionCode=None, file_path=None):
    if not valid_params(packageName, gsfId, authSubToken):
        return

    if versionCode is None:
        versionCode = get_version_number(packageName, gsfId, authSubToken)

    headers = request_helper.getDefaultHeaders(gsfId, authSubToken)
    params = {'ot': '1', 'doc': packageName, 'vc': str(versionCode)}
    response = requests.post(config.PURCHASE_URL, headers=headers, params=params, verify=True, timeout=60)

    resObj = googleplay_pb2.ResponseWrapper.FromString(response.content)
    if resObj.commands.displayErrorMessage != "":
        print "%s - [ERROR] Something went wrong" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        raise request_helper.RequestError(resObj.commands.displayErrorMessage)
    else:
        dlToken = resObj.payload.buyResponse.downloadToken
        if dlToken is not None:
            params['dtok'] = dlToken
        print "%s - Downloading package '%s', with version '%d'" % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), packageName, versionCode)
        data = delivery(headers, params)

        with open(file_path + '%s_%d.apk' % (packageName, versionCode), 'wb') as f:
            f.write(data)
            print "%s - Download successful" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')


def delivery(headers, params):
    response = requests.get(config.DELIVERY_URL, headers=headers, params=params, verify=True, timeout=60)
    resObj = googleplay_pb2.ResponseWrapper.FromString(response.content)
    if resObj.commands.displayErrorMessage != "":
        print "%s - [ERROR] something went wrong!" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        raise request_helper.RequestError(resObj.commands.displayErrorMessage)
    elif resObj.payload.deliveryResponse.appDeliveryData.downloadUrl == "":
        print "%s - [ERROR] app not purchased!" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        raise request_helper.RequestError('App not purchased')
    else:
        cookie = resObj.payload.deliveryResponse.appDeliveryData.downloadAuthCookie[0]
        cookies = {
            str(cookie.name): str(cookie.value)
        }
        downloadUrl = resObj.payload.deliveryResponse.appDeliveryData.downloadUrl
        return requests.get(downloadUrl, headers=headers, cookies=cookies, verify=True, timeout=60).content


def valid_params(packageName, gsfId, authSubToken):
    print "%s - Validating download API params" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    are_params_valid = True
    if packageName is None:
        are_params_valid = False
        print "%s - Package name has to be defined to download the package" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    if gsfId is None:
        are_params_valid = False
        print "%s - Google Services Framework Identifier (gsfId) must be defined to download the package" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    if authSubToken is None:
        are_params_valid = False
        print "%s - Auth token must be defined to download the package" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    return are_params_valid