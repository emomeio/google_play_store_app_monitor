import requests

from api.conf import device, config
from api.helpers import googleplay_pb2


class RequestError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


def executeRequestApi2(url, headers):
    response = requests.get(url, headers=headers, verify=True, timeout=60)

    message = googleplay_pb2.ResponseWrapper.FromString(response.content)
    if message.commands.displayErrorMessage != "":
        raise RequestError(message.commands.displayErrorMessage)

    return message


def getDefaultHeaders(gsfId, authSubToken):
    return {
        "Accept-Language": "en-US",
        "X-DFE-Encoded-Targets": config.DFE_TARGETS,
        "User-Agent": get_user_agent(),
        "X-DFE-Device-Id": "{0:x}".format(gsfId),
        "Authorization": "GoogleLogin auth=%s" % authSubToken
    }


def get_user_agent():
    return ("Android-Finsky/8.1.72.S-all [6] [PR] 165478484 ("
            "api=3"
            ",versionCode={versionCode}"
            ",sdk={sdk}"
            ",device={device}"
            ",hardware={hardware}"
            ",product={product}"
            "").format(versionCode=device.Vending_version,
                       sdk=device.Build_VERSION_SDK_INT,
                       device=device.Build_DEVICE,
                       hardware=device.Build_HARDWARE,
                       product=device.Build_PRODUCT)