from base64 import b64decode, urlsafe_b64encode

from Crypto.Cipher import PKCS1_OAEP
from Crypto.Hash import SHA
from Crypto.PublicKey import RSA
from Crypto.Util import asn1

import struct
import sys

from api.conf import config

VERSION = sys.version_info[0]


def encrypt_password(email, password):
    google_public_key = b64decode(config.GOOGLE_PUBKEY)
    i = read_int(google_public_key, 0)
    modulus = to_big_int(google_public_key[4:][0:i])
    j = read_int(google_public_key, i + 4)
    exponent = to_big_int(google_public_key[i + 8:][0:j])

    seq = asn1.DerSequence()
    seq.append(modulus)
    seq.append(exponent)

    public_key = RSA.importKey(seq.encode())
    cipher = PKCS1_OAEP.new(public_key)
    combined = email.encode() + b'\x00' + password.encode()
    encrypted = cipher.encrypt(combined)
    h = b'\x00' + SHA.new(google_public_key).digest()[0:4]
    return urlsafe_b64encode(h + encrypted)


def read_int(byteArray, start):
    return struct.unpack("!L", byteArray[start:][0:4])[0]


def to_big_int(byteArray):
    array = byteArray[::-1]
    out = 0
    for key, value in enumerate(array):
        if VERSION == 3:
            decoded = struct.unpack("B", bytes([value]))[0]
        else:
            decoded = struct.unpack("B", value)[0]
        out |= decoded << key * 8
    return out
