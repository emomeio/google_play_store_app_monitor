#!/usr/bin/python

from itertools import chain

import datetime
import requests

from conf import config
from helpers import mapping_helper, request_helper


def search(query, gsfId, authSubToken, limit=5):
    if not valid_params(query, gsfId, authSubToken):
        return

    print "%s - Searching for '%s'" % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), query)
    headers = request_helper.getDefaultHeaders(gsfId, authSubToken)
    next_path = "search?c=3&q=%s" % requests.utils.quote(query)

    results = []
    remaining = limit
    while remaining > 0 and next_path is not None:
        current_url = config.BASE_URL_WITH_FDFE + next_path
        data = request_helper.executeRequestApi2(current_url, headers)
        response = data.preFetch[0].response if len(data.preFetch) > 0 else data

        if response.payload.HasField('searchResponse'):
            next_path = response.payload.searchResponse.nextPageUrl
            continue

        if len(response.payload.listResponse.cluster) == 0:
            print "%s - [ERROR] Unexpected behaviour, probably expired token" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            raise request_helper.RequestError('Unexpected behaviour, probably expired token')

        cluster = response.payload.listResponse.cluster[0]
        if len(cluster.doc) == 0:
            print "%s - No results for %s" % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), query)
            break

        if cluster.doc[0].containerMetadata.nextPageUrl != "":
            next_path = cluster.doc[0].containerMetadata.nextPageUrl
        else:
            next_path = None
        apps = list(chain.from_iterable([doc.child for doc in cluster.doc]))
        results += list(map(mapping_helper.document_to_dictionary, apps))
        remaining -= len(apps)

    if len(results) > limit:
        results = results[:limit]

    print "%s - Specified limit for search results: %d" % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), limit)
    print "%s - Number of results: %d" % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), len(results))
    for a in results:
        print "%s - Result: %d '%s'" % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), results.index(a) + 1, a['docId'])

    return results


def valid_params(query, gsfId, authSubToken):
    print "%s - Validating search API params" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    are_params_valid = True
    if query is None:
        are_params_valid = False
        print "%s - [ERROR] Search query has to be defined to search" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    if gsfId is None:
        are_params_valid = False
        print "%s - [ERROR] Google Services Framework Identifier (gsfId) must be defined to search" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    if authSubToken is None:
        are_params_valid = False
        print "%s - [ERROR] Auth token must be defined to search" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    return are_params_valid