#!/usr/bin/python
import os
import sys

from ConfigParser import SafeConfigParser

import datetime

from api.details import get_version_number
from api.download import download
from api.search import search
from api.login import login


file_path = os.path.dirname(os.path.abspath(__file__)) + "/"
sys.stdout = open(file_path + 'downloader.log', 'a')

CONFIG = SafeConfigParser()
CONFIG_FILE = CONFIG.read(file_path + 'downloader_conf.ini')[0]


def run():
    if authenticators_defined() and package_name_defined():
        gsf_id = int(CONFIG.get("authentication", "gsfid"))
        auth_token = CONFIG.get("authentication", "token")
        package_name = CONFIG.get("target", "package_name")

        store_version_code = get_version_number(package_name, gsf_id, auth_token)

        last_downloaded_version = CONFIG.get("download", "last_version")
        if not last_downloaded_version or last_downloaded_version != str(store_version_code):
            download(package_name, gsf_id, auth_token, store_version_code, file_path)
            update_conf("download", "last_version", str(store_version_code))
        else:
            print "%s - New version of '%s' is NOT available" % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), package_name)

    elif credentials_defined() and search_term_defined():
        email = CONFIG.get("login", "email")
        password = CONFIG.get("login", "password")

        auth_credentials = login(email, password)
        update_conf("authentication", "gsfid", str(auth_credentials['gsfid']))
        update_conf("authentication", "token", auth_credentials['token'])

        apps = search(CONFIG.get("search", "query"), auth_credentials['gsfid'], auth_credentials['token'])
        package_name = apps[0]['docId']
        update_conf("target", "package_name", package_name)
        run()
    else:
        print "%s - [ERROR] At least login and search data OR authentication and target data " \
              "must be defined in downloader_conf.ini before the downloader can run!" % \
            datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')


def credentials_defined():
    return CONFIG.get("login", "email") and CONFIG.get("login", "password")


def search_term_defined():
    return CONFIG.get("search", "query")


def authenticators_defined():
    return CONFIG.get("authentication", "gsfid") and CONFIG.get("authentication", "token")


def package_name_defined():
    return CONFIG.get("target", "package_name")


def update_conf(section, key, value):
    if not CONFIG.has_section(section):
        CONFIG.add_section(section)
    CONFIG.set(section, key, value)
    with open(CONFIG_FILE, 'w') as f:
        CONFIG.write(f)


print "%s - Downloader initiated" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
run()
print "%s - Downloader finished" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
