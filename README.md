##Goolge Play Store App Monitor

This Google Play Store App Monitor is put together as a part of a school project to automatically download different versions of the monitored Google Play Store app. The app has to be free in order for the download to be successful!

Most of the core logic of how to make requests to Google Play Store is taken from the [Google Play Unofficial Python API](https://github.com/NoMore201/googleplay-api) project.


####PYTHON VERSION

The downloader is developed and tested on **Python 2.7.***

####INSTALL DEPENDENCIES
```
sudo pip install requests
sudo pip install pycrypto
sudo pip install protobuf
```

####SET UP DOWNLOADER

Fill in credentials and app name to monitor in **downloader_conf.ini** (an example of a correctly filled file with dummy data is available in *__downloader_conf.example__*
1. Add e-mail and password of a valid Google account as the login values to the downloader_conf.ini
2. Add the name of the app you want to download as the search target to the downloader_conf.ini
 
Run the script manually for the first time
```
./downloader.py
```
During the first run, you will most probably see that the execution was not successful due to the following message:

> Security check is needed! Try to visit **https://accounts.google.com/b/0/DisplayUnlockCaptcha** to unlock ...

![Image](images/capcha_required.jpg)

Log into Google with the credentials added to downloader_conf.ini and press the "Continue" captcha.

![Image](images/capcha_confirm.jpg)

Rerun the script manually again

```
./downloader.py
```
Verify that the downloader_conf.ini is fully filled in now and that the first download succeeded - you should find the downloaded apk from the same folder the downloader.py is located at.

Check the logs to see information about the download. All the logged information that will be printed to the standard out during the execution of downloader.py will be directed to the log file **downloader.log** that is created into the same folder where the downloader.py is located at.  

![Image](images/initial_log.jpg)

####SET UP CRON
Open your crontab file
```
crontab -e
```
Replace the [FULL_PATH_TO] below with the correct path and add the following to the crontab file to have the script running at 2AM each day.
```
0 2 * * * /usr/bin/python [FULL_PATH_TO]/downloader.py
```

You can verify that the downloader has been running from the downloader.log file. Example output when the job is set to run each minute:
![Image](images/cronjob_log.jpg)
